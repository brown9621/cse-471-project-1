# CSE 471 Project 1

Clocks - Coldplay

By - Jake Brown

The song I choose to play with my Piano components was Clocks by Coldplay.

# Piano Component
My piano reads in .wav files from the Complete Piano directory. This is done in the LoadWave Function. The files are read into a wave table which is then played in the generate function. The generate function connects to the effects component which is described below. I also utilized the attack and release envelope we created in class to help sustain the notes. 

# Effects Component

One of the effects used in this peice and is demonstrated in the Choruseffect files is the chorus effect. This was acheived by creating a delay buffer and mixing the wet and dry signals. 

